# Amazon Tracker

A full-stack web-application to track Amazon listing prices for multiple products and display tracked price hisotry as a graph.

## Requirements

- NodeJS/NPM
  - NodeMon
- Python 2.7
- Apache
- MySQL Server

## Set Up

1. Download all dependencies using `npm install` from within `front-end` and `back-end`
2. Run an Apache webserver and MySQL server
3. Import the SQL structure from `price_tracker.sql`
4. Start the back end REST API with `nodemon app.js` while inside the `back-end` folder
5. Start the front end with `ng serve` while inside the `front-end` folder.

## Hosting on a server

1. Compile the front end application using `ng build` while inside `front-end`
2. Upload everything inside the `dist` folder to your web host's `public_http`
3. Modify the SQL connection strings inside `prices.js` and `products.js` inside `back-end` to connect to your remote SQL database
4. Import the SQL structure from `price_tracker.sql`
5. Start the back end REST API with `nodemon app.js` while inside the `back-end` folder
6. Access the front end at your remote server's IP or URL.

## Adding Products

Simply put the Amazon URL into the text box in the sidebar and click Add Product.

## Updating Prices

Send a POST request to `http://localhost:8080/prices` (or your remote URL), and all prices will be updated.
