export class Product {
	productId: number;
	productName: string;
	dateAdded: Date;
	notifyPrice: number;
}
