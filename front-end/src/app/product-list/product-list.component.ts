import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from '../models/product';
import { ProductsService } from '../services/products.service';

@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: [ './product-list.component.css' ]
})
export class ProductListComponent implements OnInit {
	products: Product[];
	constructor(private productService: ProductsService) {}
	@Output() signalRelease = new EventEmitter<Product>();

	ngOnInit() {
		this.refreshProducts();
	}

	refreshProducts(): void {
		this.productService.getProducts().subscribe((products) => {
			this.products = products;
		});
	}

	releaseProduct(product: Product): void {
		this.signalRelease.emit(product);
	}

	onFormSubmit(formData: any): void {
		this.productService.addProduct(formData.product_url).subscribe((result) => {
			this.refreshProducts();
		});
	}
}
