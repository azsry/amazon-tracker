import { Component, ViewEncapsulation } from '@angular/core';
import { Product } from './models/product';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ],
	encapsulation: ViewEncapsulation.None
})
export class AppComponent {
	product: Product;

	handleEvent(product: Product): void {
		this.product = product;
	}
}
