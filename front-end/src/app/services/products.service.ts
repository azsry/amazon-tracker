import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
	providedIn: 'root'
})
export class ProductsService {
	apiUrl: string = 'http://localhost:8080';
	product: any;

	constructor(private http: HttpClient) {}

	getProducts(): Observable<Product[]> {
		return this.http.get<Product[]>(this.apiUrl + '/products');
	}

	getProduct(pId: number): Observable<Product> {
		return this.http.get<Product>(this.apiUrl + '/products/' + pId);
	}

	addProduct(url: string): Observable<string> {
		this.product = {
			productUrl: url
		};

		return this.http.post<string>(this.apiUrl + '/products/add', this.product);
	}
}
