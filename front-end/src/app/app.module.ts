import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { PriceChartComponent } from './price-chart/price-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

export class FeatureModule {}
@NgModule({
	declarations: [ AppComponent, ProductListComponent, ProductItemComponent, PriceChartComponent ],
	imports: [ BrowserModule, AppRoutingModule, HttpClientModule, NgxChartsModule, FormsModule ],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
