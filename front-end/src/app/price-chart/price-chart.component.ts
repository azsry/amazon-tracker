import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../models/product';
import { PricesService } from '../services/prices.service';

@Component({
	selector: 'app-price-chart',
	templateUrl: './price-chart.component.html',
	styleUrls: [ './price-chart.component.css' ]
})
export class PriceChartComponent implements OnInit {
	@Input() product: Product;

	// options
	colorScheme = {
		domain: [ '#5AA454', '#A10A28', '#C7B42C', '#AAAAAA' ]
	};

	multi: any[] = [];

	// options
	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = true;
	xAxisLabel = 'Date Stamp';
	showYAxisLabel = true;
	yAxisLabel = 'Price';
	timeline = true;
	autoScale = false;

	constructor(private priceService: PricesService) {}

	ngOnInit() {
		console.log(this.multi);
		this.refreshChart(this.product);
	}

	refreshChart(product: Product): void {
		this.product = product;

		this.priceService.getPricesForProduct(this.product.productId).subscribe((result) => {
			this.multi = result;
			console.log(this.multi);
		});
	}
}
