const express = require('express');
const mysql = require('mysql');
const router = express.Router();
const rp = require('request-promise');
const $ = require('cheerio');

module.exports = router;

router.get('/prices', (req, res) => {
	const queryString = 'SELECT * FROM prices';
	getConnection().query(queryString, (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}

		res.json(rows);
	});
});

router.get('/prices/:id', (req, res) => {
	console.log('Returning prices for product ' + req.params.id);
	const queryString =
		'SELECT prices.id, dateStamp, products.productId, productName, price FROM prices INNER JOIN products ON products.productId = prices.productId WHERE products.productId = ?';
	getConnection().query(queryString, [ parseInt(req.params.id) ], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}

		json = [
			{
				name: rows[0].productName,
				series: []
			}
		];

		for (var i = 0; i < rows.length; i++) {
			json[0].series[i] = {
				name: rows[i].dateStamp,
				value: rows[i].price
			};
		}

		res.json(json);
	});
});

router.post('/prices', (req, res) => {
	queryString = 'SELECT productId, productUrl from products';
	getConnection().query(queryString, (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}

		ps = [];
		prices = [];
		ids = [];
		for (var i = 0; i < rows.length; i++) {
			productUrl = rows[i].productUrl;
			ids[i] = rows[i].productId;
			ps.push(rp(productUrl));
		}

		Promise.all(ps).then((result) => {
			for (var i = 0; i < result.length; i++) {
				prices[i] = $('#priceblock_ourprice', result[i]).eq(0).text().trim().replace('$', '');

				queryString = 'INSERT INTO prices(dateStamp, productId, price) VALUES(?,?,?)';
				now = new Date();
				getConnection().query(queryString, [ now, ids[i], prices[i] ], (err, rows, fields) => {
					if (err) {
						console.log(err);
						res.sendStatus(500);
						return;
					}
				});

				console.log('Updated price for product ' + ids[i] + ' to ' + prices[i]);
			}
		});
	});
	res.json({
		success: true,
		message: 'All prices updated successfully'
	});
});

const pool = mysql.createPool({
	host: 'localhost',
	user: 'tracker',
	password: '3uxzoFNWXLaO6cza',
	database: 'price_tracker',
	connectionLimit: 10
});

function getConnection() {
	return pool;
}
