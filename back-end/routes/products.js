const express = require('express');
const mysql = require('mysql');
const router = express.Router();
const rp = require('request-promise');
const $ = require('cheerio');

router.get('/products', (req, res) => {
	console.log('Getting products');
	const queryString = 'SELECT * from products';
	getConnection().query(queryString, (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}
		res.json(rows);
	});
});

router.get('/products/:id', (req, res) => {
	const queryString = 'SELECT * from products WHERE productId = ?';
	getConnection().query(queryString, [ req.params.id ], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}
		res.json(rows);
	});
});

router.post('/products/add', (req, res) => {
	// Check if that product already exists
	queryString = 'SELECT * FROM products WHERE productUrl = ?';
	getConnection().query(queryString, [ req.body.productUrl ], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		}

		if (rows.length > 0) {
			// If yes, return 500
			console.log('Product already in database!');
			res.sendStatus(500);
			res.end();
			return;
		}

		// If no, add to database and return 200

		title = '';
		rp(req.body.productUrl)
			.then(function(html) {
				title = $('#productTitle', html).eq(0).text().trim();
				// price = $('#priceblock_ourprice', html).eq(0).text().trim().replace('$', '');
			})
			.then(function() {
				queryString = 'INSERT INTO products(productName, dateAdded, productUrl) VALUES(?,?,?)';
				now = new Date();
				getConnection().query(queryString, [ title, now, req.body.productUrl ], (err, rows, fields) => {
					if (err) {
						console.log(err);
						res.sendStatus(500);
						return;
					}
				});

				console.log('Added ' + title + ' to the database!');
				res.end();
			})
			.catch(function(err) {
				console.log('Error: ' + err);
			});
	});
});

const pool = mysql.createPool({
	host: 'localhost',
	user: 'tracker',
	password: '3uxzoFNWXLaO6cza',
	database: 'price_tracker',
	connectionLimit: 10
});

function getConnection() {
	return pool;
}

module.exports = router;
